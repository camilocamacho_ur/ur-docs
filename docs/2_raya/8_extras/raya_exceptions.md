# Raya Exceptions

All the exceptions raised by Raya Python library inherits from the exception `RayaException`.

Complete list of exceptions:

## Application Exceptions

Parent exception (inherits from `RayaException`):

| Exception | Condition |
| --- | --- |
| `RayaApplicationException` | {{exc.RayaApplicationException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaInvalidAppNameException` | {{exc.RayaInvalidAppNameException}} |
| `RayaRestfulModeException` | {{exc.RayaRestfulModeException}} |

## Value Exceptions

Parent exception (inherits from `RayaException`):

| Exception | Condition |
| --- | --- |
| `RayaValueException` | {{exc.RayaValueException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaInvalidNumericRange` | {{exc.RayaInvalidNumericRange}} |
| `RayaInvalidRGBRange` | {{exc.RayaInvalidRGBRange}} |
| `RayaInvalidHSVRange` | {{exc.RayaInvalidHSVRange}} |

## Controller Exceptions

Parent exception (inherits from `RayaException`):

| Exception | Condition |
| --- | --- |
| `RayaControllerException` | {{exc.RayaControllerException}} |

### Listener Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaListenerException` | {{exc.RayaListenerException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaListenerUnknown` | {{exc.RayaListenerUnknown}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |

### Arms Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaArmsException` | {{exc.RayaArmsException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaArmsTrajectory` | {{exc.RayaArmsTrajectory}} |

### Cameras Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaCamerasException` | {{exc.RayaCamerasException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |
| `RayaCameraNotEnabled` | {{exc.RayaCameraNotEnabled}} |

### Lidar Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaLidarException` | {{exc.RayaLidarException}} |

### Motion Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaMotionException` | {{exc.RayaMotionException}} |

### Sensors Exceptions

Parent exception (inherits from `RayaControllerException`):

| Exception | Condition |
| --- | --- |
| `RayaSensorsException` | {{exc.RayaSensorsException}} |

Child exceptions:

| Exception |  Condition |
| --- | --- |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaSensorsInvalidColorName` | {{exc.RayaSensorsInvalidColorName}} |

