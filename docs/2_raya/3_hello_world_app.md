# Hello World Application

This is the "Hello World" application used in the [Getting Started]({{tree.sdk.getting_started.cli}}) guide.

Make sure you previouslty read the [Python Application Structure]({{tree.raya.python_app_struct}}) page.

Consider the following application:

``` python
from raya import RayaApplicationBase

class RayaApplication(RayaApplicationBase):

    def setup(self):
        # Create the controllers
        self.arms = self.enable_controller('arms')
        self.voice = self.enable_controller('voice')

    def loop(self):
        # Start arm trajectory and speaking
        self.arms.predef_trajectory(arm='right', traj='hand_waving')
        self.voice.text_to_speech('Hello everyone, my name is Gary')
        # Wait until arm trajectory and speaking finish
        await self.arm.await_while_moving()
        await self.voice.await_while_speaking()
        # finish application
        self.finish_app()

    def finish(self):
        # move arm to home position
        self.arm.move_to('home')
        await self.arm.await_while_moving()
```

Let's understand it line by line.

``` python
from raya import RayaApplicationBase
```

`raya` module includes all you need to develop Raya applications. Most of the functionalities are embedded in the `RayaApplicationBase` class.

## setup()

As mentioned in the [structure](/v1/docs/gary-application-structure) page, the `setup()` method is called at the begining of the application execution, and it should include variables and controllers initializacion:

``` python
self.arms = self.enable_controller('arms')
self.voice = self.enable_controller('voice')
```

`self.arms` and `self.voice` are controller objects, and are the interface to their corresponding hardware.

## loop()

`loop()` method is repeatedly called until the finishing of the application (see the [structure](/v1/docs/gary-application-structure) page).

``` python
self.arms.predef_trajectory(arm='right', traj='hand_waving')
```

`predef_trajectory` method starts a predefined "hand_waving" movement with the right arm. See the <span style="color:darkviolet"><i>list of predefined movements (pending).</i></span>.

Next, see the [Controllers Schema]({{tree.raya.controllers_schema}})

