# ArmsController.get_trajectory_status()

<span style="color:darkviolet">(not implemented)</span>

## Reference

### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| arm_name | str | Name of the arm, see the [list of arms]({{tree.raya.basic_controllers.arms_controller.list_of_arms}}). |

### Return

<span style="color:darkviolet">(to define)</span>

## Examples

> TODO!

* * *
* * *

Go back to the [Arms Controller]({{tree.raya.basic_controllers.arms_controller.arms_controller}}) page.