# ArmsController.run_trajectory()

Execute a discrete trajectory in one of the robot's arm. This method is not blocking, so after starting the execution, you can check the status using [ArmsController.get_trajectory_status()]({{tree.raya.basic_controllers.arms_controller.synchronous_methods.get_trajectory_status}}).

> NOTE: Robot can autonomously stop the trajectory of the arms if it detects possible collisions.

## Trajectory

A trajectory is a sequence of joints positions, and the time durations that each one must take.

Consider the following trajectory:

``` python
# Positions is a dictionary, where keys are joints names, and values are list of positions.
positions={
    "shoulder": [ 0.20,  2.50],
    "elbow":    [ 0.00,  0.20],
    "wrist":    [-1.50, -2.10],
}
# Durations is a list of times
durations =     [ 1.30,  2.00])
```

That trajectory describes that the arm joints will go to positions `0.2`, `0.0` and `-1.5`, and it will wait there for `1.3` seconds. After that, joints will go to positions `2.5`, `0.2` and `-2.1`, and it will wait there for `2.0` seconds. Then, the trajectory finishes.

Trajectories can be as long as you want, you just need to ensure that all the list of positions and the list of times have the same lenght.

## Reference

### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| arm_name | str | Name of the arm, see the [list of arms]({{tree.raya.basic_controllers.arms_controller.list_of_arms}}). |
| positions | dictionaty, as described above | Positions for each arm joint. |
| durations | list of floats | Times for each positio. |

### Return

`None`

## Examples

> TODO!

* * *
* * *

Go back to the [Arms Controller]({{tree.raya.basic_controllers.arms_controller.arms_controller}}) page.