# Arms Controller

Make sure you previously read the following guides, to completely understand this page:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 1. Overview

This controller allows to control the mobile base of the robot. For more information about it, see the mobile base page of your robot in the [Robots Description](https://unlimited-robotics.document360.io/v1/docs/gary-robot-description) page.

> NOTE: Robot can autonomously stop the trajectory of the arms if it detects possible collisions.

## 2. ArmsController class reference

* `ArmsController` class
    * Synchronous methods:
        * [ArmsController.get_available_arms()]({{tree.raya.basic_controllers.arms_controller.synchronous_methods.get_available_arms}})
        * [ArmsController.get_joints_list()]({{tree.raya.basic_controllers.arms_controller.synchronous_methods.get_joints_list}})
        * [ArmsController.run_trajectory()]({{tree.raya.basic_controllers.arms_controller.synchronous_methods.run_trajectory}})
        * [ArmsController.get_trajectory_status()]({{tree.raya.basic_controllers.arms_controller.synchronous_methods.get_trajectory_status}})

## 4. Exceptions

Exceptions associated to this controller:

| Exception | Condition |
| --- | --- |
| `RayaArmsException` | {{exc.RayaArmsException}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## 4. Examples

> TODO!
    