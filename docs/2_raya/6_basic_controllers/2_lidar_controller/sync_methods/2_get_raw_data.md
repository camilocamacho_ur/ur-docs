# LidarController.get_raw_data()

Get detection information from robot's LiDAR.

## Reference

### Arguments

`None`

### Return

List with lenght `len = 1 + round((angle_max-angle_min)/angle_increment)`.

| Position | Value |
| --- | --- |
| `0` | Distance at `angle_min` |
| `1` | Distance at `angle_min + angle_increment` |
| `2` | Distance at `angle_min + 2*angle_increment` |
| ... | ... |
| `len-1` | Distance at `angle_max` |

| `angle_min` | Minimum detection angle. |
| `angle_max` | Maximum detection angle. |

## Examples

``` python
...
self.lidar = self.enable_controller('lidar')
...
raw_data = self.lidar.get_raw_data()
print(f'Lenght of raw_data: {len(raw_data)}')
print('Raw Data')
print(raw_data)
...
```

Output

```
Lenght of Raw Data: 666
Raw Data: 
[0.058476246893405914, 0.05941983312368393,  ...,  0.06039609760046005, 0.05941950902342796, 0.05847584828734398]
```

* * *
* * *

Go back to the [Lidar Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) page.

