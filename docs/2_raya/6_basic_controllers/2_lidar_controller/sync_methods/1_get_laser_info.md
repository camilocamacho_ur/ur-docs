# LidarController.get_laser_info()

Get the following LiDAR technical information:

* Minimum detection angle.
* Maximum detection angle.
* Angle increment between measurements.

## Reference

### Arguments

| Argument | Type | Default value |  |
| --- | --- | --- | --- |
| `degrees` | bool | `False` | Selector for radians (`False`) or degrees (`True`) |

### Return

Dictionary with the following keys:

| Key | Value |
| --- | --- |
| `angle_increment` | Angle increment between measurements. |
| `angle_min` | Minimum detection angle. |
| `angle_max` | Maximum detection angle. |

## Examples

### Getting Lidar information in radians

``` python
...
self.lidar = self.enable_controller('lidar')
...
lidar_info = self.lidar.get_laser_info()
print(lidar_info)
...
```

Output

```
{
    'angle_increment': 0.005774015095084906, 
    'angle_min': -1.9198600053787231, 
    'angle_max': 1.9198600053787231
}
```

### Getting Lidar information in degrees

Same sensor that previous example.

``` python
...
self.lidar = self.enable_controller('lidar')
...
lidar_info = self.lidar.get_laser_info(degrees=True)
print(lidar_info)
...
```

Output

```
{
    'angle_increment': 0.3308266957931938, 
    'angle_min': -109.99987556416437, 
    'angle_max': 109.99987556416437
}
```

* * *
* * *

Go back to the [Lidar Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) page.


