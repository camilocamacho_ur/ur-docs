# LidarController.check_obstacle()

Check the presence of an obstacle into a specified range of angles and distances.

> TODO!: Figure that shows the obstacle detection.

## Reference

### Arguments

| Argument | Type | Default value |  |
| --- | --- | --- | --- |
| `lower_angle` | float |  | Lower bound of the angles range. |
| `higher_angle` | float |  | Higher bound of the angles range. |
| `lower_distance` | float | `0.0` | Lower bound of the distance range. |
| `higher_distance` | float | `inf` | Higher bound of the distance range. |
| `degrees` | bool | `False` | Selector for radians (`False`) or degrees (`True`) |

* Both `lower_angle` and `higher_angle` must be into the range `[angle_min, angle_max]` from laser info (see [LidarController.get_laser_info()]({{tree.raya.basic_controllers.lidar_controller.synchronous_methods.get_laser_info}}))
* `higher_angle` must be higher than `lower_angle`.
* At least one of both `lower_distance` or `higher_distance` must be speficied. 
* `higher_distance` must be higher than `lower_distance`.

### Return

Boolean: `True` if obstacle found into the specified range.

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaInvalidNumericRange` | {{exc.RayaInvalidNumericRange}} |

## Examples

Check if obstacle in front of the robot (angle range: `[-15.0°, 15.0°]`), at less than a meter and a half.

``` python
...
self.lidar = self.enable_controller('lidar')
...
detection = self.lidar.check_obstacle(
                lower_angle=-15.0, 
                higher_angle=15.0, 
                lower_distance=1.5, 
                degrees=True)
...
```

* * *
* * *

Go back to the [Lidar Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) page.

