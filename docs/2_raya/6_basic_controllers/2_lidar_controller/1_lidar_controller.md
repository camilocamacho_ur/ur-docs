# Lidar Controller

Make sure you previously read the following guides, to completely understand this page:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 1. Overview

This controller allows to read obstacles perception from the robot's LiDAR. See the [Robots Description](https://unlimited-robotics.document360.io/v1/docs/gary-robot-description) page to find detailed information about the robot's LiDAR.

## 2. LidarController class reference

* `LidarController` class
    * Synchronous methods:
        * [LidarController.get_laser_info()]({{tree.raya.basic_controllers.lidar_controller.synchronous_methods.get_laser_info}})
        * [LidarController.get_raw_data()]({{tree.raya.basic_controllers.lidar_controller.synchronous_methods.get_raw_data}})
        * [LidarController.check_obstacle()]({{tree.raya.basic_controllers.lidar_controller.synchronous_methods.check_obstacle}})
    * Listener methods:
        * [CamerasController.create_obstacle_listener()]({{tree.raya.basic_controllers.lidar_controller.listener_methods.create_obstacle_listener}})
        * [CamerasController.delete_listener()]({{tree.raya.basic_controllers.lidar_controller.listener_methods.delete_listener}})

## 3. Exceptions

Exceptions associated to this controller:

| Exception | Condition |
| --- | --- |
| `RayaLidarException` | {{exc.RayaLidarException}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## 4. Examples

> TODO!
