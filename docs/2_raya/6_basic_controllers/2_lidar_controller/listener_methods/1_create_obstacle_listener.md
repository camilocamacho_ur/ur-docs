# LidarController.create_obstacle_listener()

Create a listener triggered when an object is detected into a specified range of angles and distances.

> TODO!: Figure that shows the obstacle detection (the same than check_obstacle).

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| listener_name | string |  | Unique listener identifier |
| callback | function or tuple | | Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| `lower_angle` | float |  | Lower bound of the angles range. |
| `higher_angle` | float |  | Higher bound of the angles range. |
| `lower_distance` | float | `0.0` | Lower bound of the distance range. |
| `higher_distance` | float | `inf` | Higher bound of the distance range. |
| `degrees` | bool | `False` | Selector for radians (`False`) or degrees (`True`) |

* Both `lower_angle` and `higher_angle` must be into the range `[angle_min, angle_max]` from laser info (see [LidarController.get_laser_info()]({{tree.raya.basic_controllers.lidar_controller.synchronous_methods.get_laser_info}}))
* `higher_angle` must be higher than `lower_angle`.
* At least one of both `lower_distance` or `higher_distance` must be speficied. 
* `higher_distance` must be higher than `lower_distance`.

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaInvalidNumericRange` | {{exc.RayaInvalidNumericRange}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Callback Arguments

Callback does not receive any arguments.

## Examples

> TODO!

* * *
* * *

Go back to the [Lidar Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) page.

