# LidarController.delete_listener()

Disable and delete a previously created listener.

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| listener_name | string |  | Unique listener identifier |

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerUnknown` | {{exc.RayaListenerUnknown}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

* * *
* * *

Go back to the [Lidar Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) page.
