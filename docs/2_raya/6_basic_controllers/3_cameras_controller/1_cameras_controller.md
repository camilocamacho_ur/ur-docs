# Cameras Controller

Make sure you previously read the following guides, to completely understand this page:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 1. Overview

This controller allows to get images from the robot cameras. For more information about sensors, see the cameras page of your robot in the [Robots Description](https://unlimited-robotics.document360.io/v1/docs/gary-robot-description) page.

Cameras generates two types of images:

* **RGB Image**: Color image with three channels: red, green and blue.
* **Depth image**: One channel image, where each pixel data represent distance from the camera to the scene object.

Images are represented as [Numpy Arrays](https://numpy.org/doc/stable/reference/generated/numpy.array.html).

> TODO!: Some info about the depth image and ranges.

## 2. CamerasController class reference

* `CamerasController` class
    * Synchronous methods:
        * [CamerasController.get_available_cameras()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.get_available_cameras}})
        * [CamerasController.enable_camera()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.enable_camera}})
        * [CamerasController.disable_camera()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.disable_camera}})
        * [CamerasController.take_snapshot()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.take_snapshot}})
    * Listener methods:
        * [CamerasController.create_frame_listener()]({{tree.raya.basic_controllers.cameras_controller.listener_methods.create_frame_listener}})
        * [CamerasController.delete_listener()]({{tree.raya.basic_controllers.cameras_controller.listener_methods.delete_listener}})

## 3. Exceptions

Exceptions associated to this controller:

| Exception | Condition |
| --- | --- |
| `RayaCamerasException` | {{exc.RayaCamerasException}} |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |
| `RayaCameraNotEnabled` | {{exc.RayaCameraNotEnabled}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## 4. Examples

> TODO!
