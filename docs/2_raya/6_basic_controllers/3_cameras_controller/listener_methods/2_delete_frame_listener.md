# CamerasController.delete_frame_listener()

Disable and delete a previously created frame listener [CamerasController.create_frame_listener()]({{tree.raya.basic_controllers.cameras_controller.listener_methods.create_frame_listener}}).

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| camera_name | string |  | Camera name |

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerUnknown` | {{exc.RayaListenerUnknown}} |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

* * *
* * *

Go back to the [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) page.