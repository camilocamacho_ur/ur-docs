# CamerasController.create_frame_listener()

Create a listener triggered each time that the camera generates a new frame. You can only create **one** listener per camera.

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| camera_name | str |  | Camera name |
| callback | function or tuple | | Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| compressed | boolean | False | When `True`, the listener sends a `jpeg` compressed image to the callback| 

**Important:** Camera must be previously enabled with [CamerasController.enable_camera()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.enable_camera}}).

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Callback Arguments

Callback must receive the following arguments.

| Arguments | Type |  |
| --- | --- |--- |
| frame | [Numpy array](https://numpy.org/doc/stable/reference/generated/numpy.array.html) | Captured frame with shape `(height, width, 3)` when `compressed == False`, and unidimensional (jpeg) when `compressed == True`  |

## Examples

> TODO!

* * *
* * *

Go back to the [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) page.
