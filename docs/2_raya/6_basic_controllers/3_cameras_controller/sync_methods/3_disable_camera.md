# CamerasController.disable_camera()

Disable the specified camera.

## Reference

### Arguments

| Argument | Type |  |
| --- | --- | --- |
| camera_name | string | Name of the camera to disable, see the [list of cameras]({{tree.raya.basic_controllers.cameras_controller.list_of_cameras}}) |

### Return

`None`

### Exceptions

| Exception | Condition |
| --- | --- |
| `RayaCamerasException` | {{exc.RayaCamerasException}} |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |
| `RayaCameraNotEnabled` | {{exc.RayaCameraNotEnabled}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## Examples

> TODO!

* * *
* * *

Go back to the [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) page.
