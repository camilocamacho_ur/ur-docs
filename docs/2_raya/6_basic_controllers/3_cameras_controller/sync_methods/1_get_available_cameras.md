# CamerasController.get_available_cameras()

Return the list of available cameras names.

## Reference

### Arguments

`None`

### Return

List of string, containing the names of the available cameras.


## Examples

> TODO!

* * *
* * *

Go back to the [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) page.
