# CamerasController.take_snapshot()

Take a snapshop from the selected camera.

## Reference

### Arguments

| Argument | Type | Default value |  | 
| --- | --- | --- | --- |
| camera_name | string |  | Name of the camera to disable, see the [list of cameras]({{tree.raya.basic_controllers.cameras_controller.list_of_cameras}}) |
| compressed | boolean | False | When `True`, this method returns a `jpeg` compressed image | 

**Important:** Camera must be previously enabled with [CamerasController.enable_camera()]({{tree.raya.basic_controllers.cameras_controller.synchronous_methods.enable_camera}}).

### Return

* When `compressed == False`:

[Numpy array](https://numpy.org/doc/stable/reference/generated/numpy.array.html) with shape `(height, width, 3)`, where `3` represents RGB channels. `height` and `width` depend on the selected camera, see the [list of cameras]({{tree.raya.basic_controllers.cameras_controller.list_of_cameras}}).

* When `compressed == True`:

Unidimensional [Numpy array](https://numpy.org/doc/stable/reference/generated/numpy.array.html) with `jpeg` RGB compressed image.

### Exceptions

| Exception | Condition |
| --- | --- |
| `RayaInvalidCameraName` | {{exc.RayaInvalidCameraName}} |
| `RayaCameraNotEnabled` | {{exc.RayaCameraNotEnabled}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## Examples

> TODO!

* * *
* * *

Go back to the [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) page.
