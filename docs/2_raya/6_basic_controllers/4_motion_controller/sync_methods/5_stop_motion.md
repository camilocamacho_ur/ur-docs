# MotionController.stop_motion()

Stop the robot's mobile base current command and set velocities to zero.

## Reference

### Arguments

`None`

### Return

`None`

* * *
* * *

Go back to the [Motion Controller]({{tree.raya.basic_controllers.motion_controller.motion_controller}}) page.