# MotionController.set_linear_velocity()

Set the linear velocities of the robot's mobile base. It overrides any previous command.

> NOTE: Ultrasonic Sensors and LiDAR can autonomously stop the motion of the robot if they detect possible collisions.

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| linear_velocity | float |  | Forward (positive) or backward (negative) velocity, in meters per second [m/s]  |
| time | float | 0.0 | Time (in seconds) to move at the selected velocity. If zero, the speed will be maintained until it is overwritten, an obstacle is found, or the application finishes |

### Return

`None`

## Examples

> TODO!

* * *
* * *

Go back to the [Motion Controller]({{tree.raya.basic_controllers.motion_controller.motion_controller}}) page.