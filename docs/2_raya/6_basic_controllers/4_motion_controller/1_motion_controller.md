# Motion Controller

Make sure you previously read the following guides, to completely understand this page:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 1. Overview

This controller allows to control the mobile base of the robot. For more information about it, see the mobile base page of your robot in the [Robots Description](https://unlimited-robotics.document360.io/v1/docs/gary-robot-description) page.

Robot can move forward/backward (linear velocity), rotate on its own axis (rotational velocity), or both. The following figure presents the polarity of these velocities:

> TODO!: Figure that shows the velocities representations and their polarities as arrows.

Robots uses the following units for velocities:

* Linear velocity: `m/s` (meters per second)
* Rotational velocity: `rad/s` (radians per second)

> NOTE: Ultrasonic Sensors and LiDAR can autonomously stop the motion of the robot if they detect possible collisions.

## 2. MotionController class reference

* `MotionController` class
    * Synchronous methods:
        * [MotionController.set_velocity()]({{tree.raya.basic_controllers.motion_controller.synchronous_methods.set_velocity}})
        * [MotionController.set_linear_velocity()]({{tree.raya.basic_controllers.motion_controller.synchronous_methods.set_linear_velocity}})
        * [MotionController.set_rotational_velocity()]({{tree.raya.basic_controllers.motion_controller.synchronous_methods.set_rotational_velocity}})
        * [MotionController.stop_motion()]({{tree.raya.basic_controllers.motion_controller.synchronous_methods.stop_motion}})

## 4. Exceptions

Exceptions associated to this controller:

| Exception | Condition |
| --- | --- |
| `RayaMotionException` | {{exc.RayaMotionException}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## 4. Examples

> TODO!
    