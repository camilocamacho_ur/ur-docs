# SensorsController.create_boolean_listener()

<span style="color:darkviolet">(not implemented)</span>

Create a listener triggered when the value of one (or multiple) ***boolean sensor(s)*** go(es) into a specific logic state.

> ***Note:*** This method only works with ***boolean sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

## Reference

### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| listener_name | string | Unique listener identifier |
| callback | function or tuple | Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| sensor_paths | string | Paths of the sensors, separated by comma, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| logic_state | bool | Logic state to trigger the listener |

### Return

`None`

## Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |

### Callback Arguments

Callback does not receive any arguments.

## Examples

<span style="color:darkviolet"><i>Pending</i></span>

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
