# SensorsController.delete_listener()

Disable and delete a previously created listener.

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| listener_name | string |  | Unique listener identifier |

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerUnknown` | {{exc.RayaListenerUnknown}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
