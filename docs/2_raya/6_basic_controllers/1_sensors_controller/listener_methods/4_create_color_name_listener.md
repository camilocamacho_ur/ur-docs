# SensorsController.create_color_name_listener()

<span style="color:darkviolet">(not implemented)</span>

Create a listener triggered when the value of one (or multiple) ***color sensor(s)*** detects the specified color (from a predefined [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}).

> ***Note:*** This listener only works with ***color sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

### Reference

#### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| listener_name | string | Unique listener identifier |
| callback | function or tuple | Callback, see Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| sensor_paths | string | Paths of the sensors, separated by comma, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| color_name | string | Color name, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |

#### Return

`None`

## Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaSensorsInvalidColorName` | {{exc.RayaSensorsInvalidColorName}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Callback Arguments

Callback does not receive any arguments.

### Examples

<span style="color:darkviolet"><i>Pending</i></span>

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
