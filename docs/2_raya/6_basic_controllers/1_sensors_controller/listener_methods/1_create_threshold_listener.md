# SensorsController.create_threshold_listener()

Create a listener triggered when the value of one (or multiple) ***continuous sensor(s)*** go(es) inside/outside a specific numeric range   .

> ***Note:*** This method only works with ***continuous sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

## Reference

### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| listener_name | string |  | Unique listener identifier |
| callback | function or tuple | | Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| sensor_paths | list of strings |  | List of sensors paths, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| lower_bound | -inf | float | Lower limit of the window |
| higher_bound | inf | float | Higher limit of the window |
| inside_range | True | boolean | Triggers when sensor value goes inside (True) / outside (False) the range  |
| abs_val | False | float | If true, uses the absolute value of the sensor value |

At least one of both `lower_bound` or `higher_bound` must be speficied. 

Note that if `abs_val` is True, `lower_bound` or/and `higher_bound` must be positive to have effect.

### Return

`None`

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaInvalidNumericRange` | {{exc.RayaInvalidNumericRange}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Callback Arguments

Callback does not receive any arguments.

## Examples

### 1. Detecting fast rotation

> TODO!: list of paths

The following code detects when the robot rotates faster than 0.7rad/s (in any direction), and executes the `listener_rotation_callback` function asynchronously.

``` python
async def setup(self):
    ...
    self.sensors = self.enable_controller('sensors')
    self.sensors.create_threshold_listener(listener_name='listener_rotation',
                                           callback = (self, 'listener_rotation_callback'),
                                           sensor_paths = '/imu/rot_vel/z',
                                           lower_bound = 0.7,
                                           abs_val=True)
    ...
    
async def loop(self):
    # Doing other stuff
    ...
    
def listener_rotation_callback(self):
        print('rotating too fast!!!')
```

### 2. Obstacles from multiple ultrasonic sensors

> TODO!: list of paths

The following code detects obstacles between 0.5m and 1.5m, in any of the three ultrasonic sensors,  and 

``` python
async def setup(self):
    ...
    self.sensors = self.enable_controller('sensors')
    self.sensors.create_threshold_listener(listener_name='obstacles',
                                               callback = (self, 'obstacles_callback'),
                                               sensor_paths = '/sonar/left,/sonar/front,/sonar/right',
                                               lower_bound = 0.5,
                                               lower_bound = 1.5)
    ...
    
async def loop(self):
    # Doing other stuff
    ...
    
def obstacles_callback(self):
        print('obstacle detected!')
```

### 3. External temperature outside specified range

> TODO!

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
