# SensorsController.create_color_range_listener()

<span style="color:darkviolet">(not implemented)</span>

Create a listener triggered when the value of one (or multiple) ***color sensor(s)*** go(es) into a specific range of RGB or HSV values (inclusive).

> ***Note:*** This listener only works with ***color sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

## Reference

### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| listener_name | string | Unique listener identifier |
| callback | function or tuple | Callback (see the section *Callback* in the Callback (see the [Callback section in the Listeners Schema page]({{tree.raya.listeners_schema}}#2-callbacks)) |
| sensor_paths | string | Paths of the sensors, separated by comma, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| low_rgb | tuple of three int | Lower limits in RGB color space |
| high_rgb | tuple of three int | Higher limits in RGB color space |
| low_hsv | tuple of three float | Lower limits in HSV color space |
| high_hsv | tuple of three float | Higher limits in HSV color space |

Three values of `low_rgb` (or low_hsv) must be lower than `low_hsv` (or `high_hsv`).

Only one pair between `low_rgb` and `high_rgb`, or `low_hsv` and `high_hsv`, must be specified.

### Return

`None`

## Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaInvalidRGBRange` | {{exc.RayaInvalidRGBRange}} |
| `RayaInvalidHSVRange` | {{exc.RayaInvalidHSVRange}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Callback Arguments

Callback does not receive any arguments.

## Examples

<span style="color:darkviolet"><i>Pending</i></span>

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
