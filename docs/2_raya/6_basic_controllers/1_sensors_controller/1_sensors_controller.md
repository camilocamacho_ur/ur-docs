# Sensors Controller

Make sure you previously read the following guides, to completely understand this page:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 1. Overview

This controller allows to read most of the sensors that the Robot has. For more information about sensors, see the sensor page of your robot in the [Robots Description](https://unlimited-robotics.document360.io/v1/docs/gary-robot-description) page.

UR Robots have three types of sensors, according to the type of data they output:

* ***Continuous sensors:*** Read variable is a decimal continuous value.
* ***Boolean sensors:*** Read variable is a binary state.
* ***Color sensors:*** Read variable is a color in the RGB color space.

Sensors are identified by a unique ***sensor path***. See the complete [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) for information about ranges, units and paths of the avaliable sensors.

## 2. SensorsController class reference

* `SensorController` class
    * Synchronous methods:
        * [SensorsController.get_sensor_value()]({{tree.raya.basic_controllers.sensors_controller.synchronous_methods.get_sensor_value}})
        * [SensorsController.get_all_sensors_values()]({{tree.raya.basic_controllers.sensors_controller.synchronous_methods.get_all_sensors_values}})
        * [SensorsController.check_sensor_in_range()]({{tree.raya.basic_controllers.sensors_controller.synchronous_methods.check_sensor_in_range}})
        * [SensorsController.check_color_sensor_in_range()]({{tree.raya.basic_controllers.sensors_controller.synchronous_methods.check_color_sensor_in_range}})
        * [SensorsController.check_color_sensor_name()]({{tree.raya.basic_controllers.sensors_controller.synchronous_methods.check_color_sensor_name}})
    * Listener methods:
        * [SensorsController.create_threshold_listener()]({{tree.raya.basic_controllers.sensors_controller.listener_methods.create_threshold_listener}})
        * [SensorsController.delete_threshold_listener()]({{tree.raya.basic_controllers.sensors_controller.listener_methods.delete_threshold_listener}})
        * [SensorsController.create_boolean_listener()]({{tree.raya.basic_controllers.sensors_controller.listener_methods.create_boolean_listener}})
        * [SensorsController.create_color_range_listener()]({{tree.raya.basic_controllers.sensors_controller.listener_methods.create_color_range_listener}})
        * [SensorsController.create_color_name_listener()]({{tree.raya.basic_controllers.sensors_controller.listener_methods.create_color_name_listener}})

## 3. Exceptions

Exceptions associated to this controller:

| Exception | Condition |
| --- | --- |
| `RayaSensorsException` | {{exc.RayaSensorsException}} |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaSensorsInvalidColorName` | {{exc.RayaSensorsInvalidColorName}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## 4. Examples

> TODO!
    