# SensorsController.get_all_sensor_values()

Synchronously get current state of all the sensors.

### Reference

#### Arguments

`None` 

#### Return

Dictionary with current values of all the sensors, where keys are paths. 

### Examples

#### Reading all the sensors

``` python
...
self.sensors = self.enable_controller('sensors')
...
all_sensors_info = self.sensors.get_all_sensors_values()
print(all_sensors_info)
...
```

Output

``` python
{
    "/temperature": 18.2,
    "/pressure": 0.55,
    "/imu/lin_acc/x": 0.0024,
    "/imu/lin_acc/y": 0.0438,
    "/imu/lin_acc/z": 0.9763,
    "/imu/rot_vel/x": 0.0234,
    "/imu/rot_vel/y": 0.0031,
    "/imu/rot_vel/z": 0.2302,
    "/sonar/rear_left": 0.25,
    "/sonar/rear_center": 0.12,
    "/sonar/rear_right": 0.54,
    "/line_sensor/left": False,
    "/line_sensor/center": True,
    "/line_sensor/right": True,
    "/color_sensor/hand":(240,45,2)
}
```

***
***

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.