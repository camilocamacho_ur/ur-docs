# SensorsController.get_sensor_value()

Synchronously get current value of a sensor.

## Reference

### Arguments

| Argument | Type |  |
| --- | --- | --- |
| sensor_path | string | Path of the sensor to read, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |

### Return

Curent value of the sensor.

Return value type depends on the type of sensor:

| Type of sensor | Type of return value |
| --- | --- |
| Continuous sensor | float |
| Boolean sensor | bool |
| Color sensor | Tuple of 3 elements (R,G,B) |

### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

## Examples

### Reading temperature

> Works with any continuous sensor

``` python
...
self.sensors = self.enable_controller('sensors')
...
temp = self.sensors.get_sensor_value('/temperature')
print(f'Temperature = {temp}C')
print(f'Data type = {type(temp)}')
...
```

Output:
```
Temperature = 19.4C
Data type = <class 'float'>
```

### Reading center line sensor

> Works with any boolean sensor

``` python
...
self.sensors = self.enable_controller('sensors')
...
center_line = self.sensors.get_sensor_value('/line_sensor/center')
print(f'Line state = {center_line}')
print(f'Data type = {type(center_line)}')
...
```

Output:
```
Line state = False
Data type = <class 'bool'>
```

### Reading hand color sensor

> Works with any color sensor

``` python
...
self.sensors = self.enable_controller('sensors')
...
object_color = self.sensors.get_sensor_value('/color_sensor/hand')
print(f'Object color = {object_color}')
print(f'Data type = {type(object_color)}')
...
```

Output:
```
Object color = (243, 12, 67)
Data type = <class 'tuple'>
```

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.
