# SensorsController.check_color_sensor_in_range()

<span style="color:darkviolet">(not implemented)</span>

Check if the current value of one ***color sensor*** is into a specific range of RGB or HSV values (inclusive).

> ***Note:*** This method only works with ***color sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

### Reference

#### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| sensor_path | | string | Sensor path, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| low_rgb | tuple of three int | Lower limits in RGB color space |
| high_rgb | tuple of three int | Higher limits in RGB color space |
| low_hsv | tuple of three float | Lower limits in HSV color space |
| high_hsv | tuple of three float | Higher limits in HSV color space |

Three values of `low_rgb` (or low_hsv) must be lower than `low_hsv` (or `high_hsv`).

Only one pair between `low_rgb` and `high_rgb`, or `low_hsv` and `high_hsv`, must be specified.

#### Return

Boolean: `True` if the detected color is into the specified range

#### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaInvalidRGBRange` | {{exc.RayaInvalidRGBRange}} |
| `RayaInvalidHSVRange` | {{exc.RayaInvalidHSVRange}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Examples

Check if the detected color is around "orange". The H range for orange is aproximately between 7.5° and 22.5°. Reasonable ranges for S and V are values greater than 50%. ([HSV color space](https://en.wikipedia.org/wiki/HSL_and_HSV)).

``` python
...
self.sensors = self.enable_controller('sensors')
...
if self.sensors.check_color_sensor_in_range('/color_sensor/hand', low_hsv=(7.5, 50, 50), high_hsv=(22.5, 100, 100)):
    print('Color of the object in the robot\'s hand is ORANGE')
else:
    print('Color of the object in the robot\'s hand is NOT ORANGE')
...
```

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.