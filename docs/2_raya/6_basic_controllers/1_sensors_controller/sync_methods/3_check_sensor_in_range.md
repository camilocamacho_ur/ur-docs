# SensorsController.check_sensor_in_range()

<span style="color:darkviolet">(not implemented)</span>

Check if the current value of a ***continuous sensor*** is inside/outside a range.

> ***Note:*** This method only works with ***continuous sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

### Reference

#### Arguments

| Arguments | Type | Default value |  |
| --- | --- | --- | --- |
| sensor_path | | string | Sensor path, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| lower_bound (optional) | -inf | float | Lower bound of the range |
| higher_bound (optional) | inf | float (optional) | Higher limit of the range |
| inside_range | True | boolean (optional) | Detects when sensor value is inside (True) / outside (False) the range  |
| abs_val (optional) | False | float (optional) | If true, uses the absolute value of the sensor value |

At least one of both `lower_bound` and `higher_bound` must be speficied. 

Note that if `abs_val` is True, `lower_bound` or/and `higher_bound` must be positive to have effect.

#### Return

Boolean. `True` if the value of the sensor inside (`inside_range=True`) / outside (`inside_range=False`) the specified range.

#### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaInvalidNumericRange` | {{exc.RayaInvalidNumericRange}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Examples

Chek if there are any object behind the robot (using ultrasonic sensor), less than 30 centimeters:

``` python
...
self.sensors = self.enable_controller('sensors')
...
if self.sensors.get_all_sensors_values("/sonar/rear_center", lower_bound=0.3):
    print('Obstacle detected!')
else:
    print('No obstacle')
...
```

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.