# SensorsController.check_color_sensor_name()

<span style="color:darkviolet">(not implemented)</span>

Check if the current value of one ***color sensor*** corresponds to a predefined color (from the [list of colors]({{tree.raya.extras.list_of_colors}})).

> ***Note:*** This method only works with ***color sensors***. Remember the types of sensors in the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.

### Reference

#### Arguments

| Arguments | Type |  |
| --- | --- | --- |
| sensor_path | | string | Sensor path, see the [list of sensors]({{tree.raya.basic_controllers.sensors_controller.list_of_sensors}}) |
| color_name | string | Color name, see the [list of colors]({{tree.raya.extras.list_of_colors}}) |

#### Return

Boolean: `True` if the detected color correspond to the specified color.

#### Exceptions

| Exception |  Condition |
| --- | --- |
| `RayaSensorsUnknownPath` | {{exc.RayaSensorsUnknownPath}} |
| `RayaSensorsIncompatiblePath` | {{exc.RayaSensorsIncompatiblePath}} |
| `RayaSensorsInvalidPath` | {{exc.RayaSensorsInvalidPath}} |
| `RayaSensorsInvalidColorName` | {{exc.RayaSensorsInvalidColorName}} |

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

### Examples

Check if the detected color is aroud "orange". The H range for orange is aproximately between 7.5° and 22.5°. Reasonable ranges for S and V are values greater than 50%. ([HSV color space](https://en.wikipedia.org/wiki/HSL_and_HSV)).

``` python
...
self.sensors = self.enable_controller('sensors')
...
if self.sensors.check_color_sensor_name('/color_sensor/hand', 'red'):
    print('Color of the object in the robot\'s hand is RED')
else:
    print('Color of the object in the robot\'s hand is NOT RED')
...
```

* * *
* * *

Go back to the [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) page.