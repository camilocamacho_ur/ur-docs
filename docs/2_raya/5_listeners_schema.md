# Listeners Schema

> Raya API is embedded in the `RayaApplicationBase` class (see the [Raya Application Structure]({{tree.raya.python_app_struct}}) page). All needed functions to access the robot funtionalities are methods of that class.

Listeners allows to asynchronously check the robot peripherals (sensors, cameras, actuators, etc), through a events-like mechanism. Listeners are created by controllers according to its specific functionalities.

## 1. Why listeners?

### Synchronous example

Consider the following application to make Gary to greet when it detects someone in front of it:

``` python
from garyapi import GaryApplicationBase

class GaryApplication(GaryApplicationBase):
    async def setup(self):
        self.ai_vision = self.enable_controller('ai_vision')
        self.voice = self.enable_controller('voice')
        self.arms = self.enable_controller('arms')

    async def loop(self):
        # Check for a person in front of the robot
        if(self.ai_vision.check_person_in_front()):
            # Start arm trajectory and speaking
            self.arm.predef_trajectory('right_hand_waving')
            self.voice.text_to_speech('Hello. How are you?')
            # Wait until arm trajectory and speaking finish
            await self.arm.await_while_moving()
            # Recovery initial robot position
            self.arm.move_to('home')
        # ...
        # Other application stuff
        # ...

    async def finish(self):
        self.motion.stop_motion()
    
```

This application needs to be polling the `check_person_in_front()` method all the time, because the `loop()` method needs to be aware of a person in front of the robot. When the condition is met, arm sequence and speaking starts, and the `loop()` method gets busy until the arm reaches the end of the trajectory. Then, the arm goes back to the initial position.

You could add extra functionalities to the program after the polling condition, but they will be paused during the execution of the greeting.

### Listeners-based example

If we would want to create the same application using listeners, it would look like:

``` python 
from garyapi import GaryApplicationBase

class GaryApplication(GaryApplicationBase):
    async def setup(self):
        self.ai_vision = self.enable_controller('ai_vision')
        self.voice = self.enable_controller('voice')
        self.arms = self.enable_controller('arms')
        # Listeners
        self.ai_vision.create_object_detection_listener(object="person",
                                                       callback=(this, "check_person_callback"))
        self.arms.create_finished_trajectory_listener(arm_name="right",
                                                      callback=(this, "end_arms_mov_callback"))

    async def loop(self):
        # ...
        # Other application stuff
        # ...

    async def finish(self):
        self.motion.stop_motion()
    
    def check_person_callback():
        self.arm.predef_trajectory('right_hand_waving')
        self.voice.text_to_speech('Hello. How are you?')
    
    def end_arms_mov_callback():
        self.arm.move_to('home')
```

After enabling the controllers, listeners are created with unique names, it should happen in the `setup()` method. Each controller has a number of listeners acording to the possible conditions that its associated hardware can meet.

In the example, a `check_person` listener is created, and it will be triggered when the frontal camera of the robot detects a person. The controller automatically checks the condition and calls the callback when it is met. The callback, which is a method called `check_person_callback()`, executes the `predef_trajectory()` and `text_to_speech()`. In simple words, the robot start greeting when it detects the person in front of it.

The second listener is called `end_arms_mov`. It is triggered when a previously started arm trajectory finishes. When the callback `end_arms_mov_callback()` is executed, it brings the arm to its original position.

The functionality of the last application is the same than the first one, but now you have an empty `loop()` method, and procedures you execute there will not be interrupted.

## 2. Callbacks

> TODO!:

## 3. Exceptions

Listeners creation process can throw the following exceptions:

| Exception | Condition |
| --- | --- |
| `RayaListenerException` | {{exc.RayaListenerException}} |
| `RayaListenerAlreadyCreated` | {{exc.RayaListenerAlreadyCreated}} |
| `RayaListenerUnknown` | {{exc.RayaListenerUnknown}} |
| `RayaInvalidCallback` | {{exc.RayaInvalidCallback}} |

In addition, each specific controller can throw specific exceptions (see each controller documentation for more detailed information).

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).


## 4. Syncronization

> TODO!:

## 5. Blocking calls

> TODO!:

Now, you can go back to [Gary Python API]({{tree.raya.raya}}) page and explore the controllers.
