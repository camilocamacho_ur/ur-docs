# Raya - Python Library

## 1. What is it?

Raya is a Python library that allows you to create Raya Applications with Python {{min_python_ver}} or newer. With this library you can:

* Access information and data from the [Sensors](https://unlimited-robotics.document360.io/v1/docs/sensors).
* Send movement commands to the [Mobile Base](https://unlimited-robotics.document360.io/v1/docs/mobile-base) to navigate the environment.
* Control the [Arms](https://unlimited-robotics.document360.io/v1/docs/arms).
* Call advanced functionalities and skills of Unlimited Robotics robots.

Make sure you [installed the SDK]({{tree.sdk.installation}}), and followed at least one of the getting started guides: [Command Line Interface]({{tree.sdk.getting_started.cli}}) or [VSCode]({{tree.sdk.getting_started.vscode}}).

## 2. Raya Python Library Overview

Read the following articles to get started with the Raya Python Library:

* [Raya Application Structure]({{tree.raya.python_app_struct}})
* [Hello World Application]({{tree.raya.hello_world_app}})
* [Controllers Schema]({{tree.raya.controllers_schema}})
* [Listeners Schema]({{tree.raya.listeners_schema}})

## 2. Controllers for basic functionalities

Controllers to interact directly with the Robot's hardware.

Perception:

| Submodule | Controller Name | Description |
| --- | --- | --- |
| [Sensors Controller]({{tree.raya.basic_controllers.sensors_controller.sensors_controller}}) | `sensors` | Temperature, Humidity, Pressure, etc. |
| [LiDAR Controller]({{tree.raya.basic_controllers.lidar_controller.lidar_controller}}) | `lidar` | Obstacle detection, mapping and navigation |
| [Cameras Controller]({{tree.raya.basic_controllers.cameras_controller.cameras_controller}}) | `cameras` | Visual perception |

Actuators:

| Submodule | Controller Name | Description |
| --- | --- | --- |
| [Motion Controller]({{tree.raya.basic_controllers.motion_controller.motion_controller}}) |  `motion` | Access to the mobile base of the robot |
| [Arms Controller]({{tree.raya.basic_controllers.arms_controller.arms_controller}}) | `arm` | Arms movement |
| Voice & Sound Controller | `voice` | Text to speech and sound engine |
| LEDs Controller | `leds` | Control of the Robot's LEDs colors and intensities |

## 3. Controllers for embedded skills

Controllers to access the robot's embeded skills.

| Submodule | Controller Name | Description |
| --- | --- | --- |
| Navigation and Mapping |  |  |
| Arms Planning and Manipulation |  |  |
| Artificial Vision |  |  |
| Voice Recognition |  |  |
| Conversational |  |  |