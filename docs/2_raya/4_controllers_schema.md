# Controllers Schema

> Raya API is embedded in the `RayaApplicationBase` class (see the [Raya Application Structure]({{tree.raya.python_app_struct}}) page). All needed functions to access the robot funtionalities are methods of that class.

## Raya Controllers

Raya Applications use *Raya Controllers*. A controller is a bridge to access specific functionalities of the robot. Controllers are enabled through the `enable_controller()` method, and it only works if the applications has the permissions to access the specific functionality (see the [Application Manifest](/v1/docs/application-manifest) page).

Controllers are identified by a unique name, and it is all you need to create them.

The controllers should be created in the `setup()` method:

``` python
from garyapi import GaryApplicationBase

class GaryApplication(GaryApplicationBase):

    async def setup(self):
        ...
        # Controller for motion of Gary
        self.motion = self.enable_controller('motion')
        # Controller for Gary's Text to Speech engine
        self.voice = self.enable_controller('voice')
        # Controller for Gary's Sensors
        self.sensors = self.enable_controller('sensors')
        # Controller for Gary's Cameras
        self.cameras = self.enable_controller('cameras')
        ...
```

`enable_controller()` is a inherited method of `GaryApplication` class, and it returns a *controller object* that contains methods according to its functionality. For instance:

``` python
        # Gary starts moving fordward at 0.5 m/s
        self.motion.set_forward_velocity(0.5)
        # Gary says something
        self.voice.text_to_speech('hello from voice controller')
```

Also, you can synchronously retrieve information from the Gary Python API:

``` python
        # Read the temperature sensor
        temp = self.sensors.get_sensor_value('/temperature/external')
        # Take a picture from the robot's head camera
        img = self.cameras.get_snapshot(camera='head')
```

These methods allows you to get information through a *polling* mechanism. To learn the way to asynchronously read sensors and retrieve information from Gary API through a event-like mechanism, see the next page: [Listeners Schema]({{tree.raya.listeners_schema}}).

## Exceptions

Controllers creation process can throw the following exceptions:

| Exception | Condition |
| --- | --- |
| `RayaControllerException` | {{exc.RayaControllerException}} |

In addition, each specific controller can throw specific exceptions (see each controller documentation for more detailed information).

See the [complete list of Raya Exceptions]({{tree.raya.extras.raya_exceptions}}).

To see the complete controllers list, go back to the [Raya Python API]({{tree.raya.raya}}) page.