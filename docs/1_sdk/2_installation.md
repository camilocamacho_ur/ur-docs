# SDK - Installation

## Anaconda / Miniconda instalation

It is highly recommended to run the Ra-Ya SDK on a conda environment. You can install [*Anaconda*](https://www.anaconda.com/products/individual#Downloads) or [*Miniconda*](https://conda.io/projects/conda/en/latest/user-guide/install/index.html), on Windows, MacOS or Linux.

## Virtual environment creation

Ra-Ya SDK and Ra-Ya Python Library run on Python 3.8 or newer. It's recommended to create a new clean virtual environment for Ra-Ya development.

Open a Anaconda terminal and run:

``` bash
conda create --name raya python=3.8
```

Then, activate the created environment:

``` bash
conda activate raya
```

And install the `rayasdk` package from the PyPi repositories:

``` bash
pip install rayasdk
```

You can install any other packages that you application needs.

Now, you are ready to [create a `helloworld` example]({{tree.sdk.getting_started.cli}}) with the RaYa SDK.
