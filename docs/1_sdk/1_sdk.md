# Software Development Kit

Ra-Ya SDK (Software Development Kit) is a set of tools that will help you create Ra-Ya Applications from you personal computer.

Raya SDK allows:

* Create Raya projects and application files system.
* Connect to Unlimited Robotics robots in Developer Mode
* Upload applications to Raya App Store
* <span style="color:darkviolet"><i>more</i></span>.

<span style="color:darkviolet"><i>Pending</i></span>.

### Content

* [Installation]({{tree.sdk.installation}})
* Getting Started
    * [Command Line Interface]({{tree.sdk.getting_started.cli}})
    * [Visual Studio Code]({{tree.sdk.getting_started.vscode}})
* [Command Line Interface - Reference]({{tree.sdk.cli_reference}})
