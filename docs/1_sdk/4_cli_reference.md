# Command Line Interface - Reference

The Command Line Interface of the Ra-Ya  SDK is a terminal tool that allows:

* Create templates for Raya applications.
* Discover robots connected to your local network.
* Connect applications in development to real robots and simulator.

Usage:

``` bash
$ ursdk [-h] [-v | -q] command {command-options}
```

Optional arguments:

* `-h`, `--help`: show this help message and exit.
* `-v`, `--verbose`: increase output verbosity.
* `-q`, `--quiet`: don't print on stdout.

Positional arguments:

* `command`: SDK Command

## Commands

### `init`

Initialize Raya project in current folder (current folder must be empty).

Usage: 

``` bash
$ ursdk init [-h] --app-id APP_ID [--app-name APP_NAME]
```

Required arguments:

* `--app-id APP_ID`: application unique identificator

Optional arguments:

* `--app-name APP_NAME`: application name
* `-h`, `--help`: show this help message and exit

Example:

``` bash
$ urdsk init --app-id helloworld --app-name 'Hello World'
```

### `scan`

Discover robots in the local network.

Usage:

``` bash
$ ursdk scan [-h]
```

Optional arguments:

* `-h`, `--help`: show this help message and exit.

Example:

``` bash
$ ursdk scan
```

Output:

```
Scanning local network for robots...

Robot ID        IP Address       DDS Channel
--------------  -------------  -------------
GARY_KITCHEN    193.168.20.36              3
GARY_2665232    193.168.20.76              1
GARY_RECEPTION  193.168.20.12              2
```

This command creates the file `${OS_TMP_FOLDER}/ursdk/last_scanning.json`with the scanning information.

### `connect`

Connect current Raya proyect to a robot or simulator.

Usage:

``` bash
$ ursdk connect [--robot-id ROBOT_ID | --simulator]
```

Required mutually exclusive arguments:

* `--robot-id ROBOT_ID`: robot identificator (from scan list).
* `--simulator`: connect the project to the simulator.

Optional arguments:

* `-h`, `--help`: show this help message and exit.

### `run`

Execute current Raya project.

Usage:

``` bash
$ ursdk run [--local]
```

Optional arguments:

* `--local`: executes the application inside the robot.
* `-h`, `--help`: show this help message and exit.