# SDK - Command Line Interface

This guide presents the creation of a "Hello World" aplication using the Ra-Ya SDK Command Line Interface. To get deeper information about the CLI tool, visit the [CLI Reference]({{tree.sdk.cli_reference}}) page.

### 1. Project creation

Open an anaconda terminal and activate the `raya` environment:

``` bash
conda activate raya
```

Then, go to an empty directory (project directory).

``` bash
# Linux/MAC example
mkdir -p ~/raya_projects/helloworld
cd ~/raya_projects/helloworld
```

Initialize a Raya project:

``` bash
rayasdk init --app-id "helloworld" --app-name "Hello World"
```

The following directory structure will be created.

```
.
├── dat
├── doc
├── exec_info.json
├── log
├── __main__.py
├── res
└── src
    ├── app.py

6 directories, 18 files
```

### 2. Hello World source code

Replace the content of the file `src/app.py` with the following code:

<span style="color:darkviolet"><i>The following code is an example that actually runs on the current simulator:</i></span>

``` python
from raya.application_base import RayaApplicationBase

class RayaApplication(RayaApplicationBase):
    async def setup(self):
        self.i = 0
        self.motion = self.enable_controller('motion')

    async def loop(self):
        self.log(f'Going to the kitchen')
        self.motion.set_velocity(linear_velocity=0.5, rotational_velocity=0.0)
        await self.sleep(4.8)
        self.motion.set_velocity(linear_velocity=0.0, rotational_velocity=3.1416)
        await self.sleep(2.0)
        self.motion.set_velocity(linear_velocity=0.5, rotational_velocity=0.0)
        await self.sleep(7.5)
        self.motion.set_velocity(linear_velocity=0.0, rotational_velocity=-3.1416)
        await self.sleep(2.0)
        self.motion.set_velocity(linear_velocity=0.5, rotational_velocity=0.0)
        await self.sleep(4.0)
        await self.finish_app()

    async def finish(self):
        self.log('Now in the kitchen, clossing app.')
        self.motion.stop_motion()
    
```

<span style="color:darkviolet"><i>The following code is the helloworld that I want to mantain when simulator get all the functionalies:</i></span>

```python
from raya import RayaApplicationBase

class RayaApplication(RayaApplicationBase):

    def setup(self):
        # Create the controllers
        self.arms = self.enable_controller('arms')
        self.voice = self.enable_controller('voice')

    def loop(self):
        # Start arm trajectory and speaking
        self.arms.predef_trajectory(arm='right', traj='hand_waving')
        self.voice.text_to_speech('Hello everyone, my name is Gary')
        # Wait until arm trajectory and speaking finish
        await self.arm.await_while_moving()
        await self.voice.await_while_speaking()
        # finish application
        self.finish_app()

    def finish(self):
        # move arm to home position
        self.arm.move_to('home')
        await self.arm.await_while_moving()
```

See the [Hello World Application](/v1/docs/hello-world-application) for a extended explanation of this source code.

### 3. Application execution

#### 3.1. Simulator

Make sure you previusly opened the simulator, then enter (only once):

``` bash
$ rayasdk connect --simulator
```

If it runs successfully, you can now execute the application just running:

``` bash
$ rayasdk run
```

#### 3.1. Real robot

Discover robots in your local network (robot must have the *developer mode* <span style="color:darkviolet"><i>(pending)</i></span> enabled to be found by the scanner):

``` bash
$ rayasdk scan
```

You should get an output like:

```
Scanning local network for robots...

Robot ID        IP Address       DDS Channel
--------------  -------------  -------------
GARY_KITCHEN    193.168.20.36              3
GARY_2665232    193.168.20.76              1
GARY_RECEPTION  193.168.20.12              2
```

Connect your application to the robot you want to test on (only once):

``` bash
$ rayasdk connect --app-id GARY_2665232
```

If it runs successfully, execute the application:

``` bash
$ rayasdk run
```