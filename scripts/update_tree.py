import yaml
import os

BASE_FILE = './mkdocs_base.yml'
OUTPUT_FILE = './mkdocs.yml'

BASE_TREE_FILE = './data/doc_tree_base.yml'
OUTPUT_TREE_FILE = './data/doc_tree.yml'

BASIC_CONTROLLERS_PATH = '2_raya/6_basic_controllers'

def dir_list(path):
    path = f'./docs/{path}'
    l = []
    for x in os.scandir(path):
        if(x.is_dir()):
            l.append(x.name)
    return sorted(l)

def md_list(path):
    path = f'./docs/{path}'
    l = []
    for x in os.scandir(path):
        if(x.name[-3:]=='.md'):
            l.append(x.name)
    return sorted(l)

def fix_name(name, cap=False):
    name_split = name.split('_')
    if(name_split[0].isnumeric()):
        name = name[len(name_split[0])+1:]
    if cap:
        name = name.replace('_', ' ')
        name = name.capitalize()
    return name.split('.')[0]


with open(BASE_FILE) as file:
    content = yaml.load(file, Loader=yaml.FullLoader)
    basic_controllers = content['nav'][2]['Raya - Python Library'][5]['Basic Controllers']

    for path in dir_list(BASIC_CONTROLLERS_PATH):
        full_path = f'{BASIC_CONTROLLERS_PATH}/{path}'
        controller = fix_name(path, cap=True)
        basic_controllers.append({controller:[]})
        this_controller = basic_controllers[-1][controller]
        for page in md_list(full_path):
            this_controller.append({fix_name(page, cap=True):f'{full_path}/{page}'})
        if os.path.isdir(f'./docs/{full_path}/sync_methods'):
            this_controller.append({'Synchronous methods':[]})
            for method in md_list(f'{full_path}/sync_methods'):
                method_path = f'{full_path}/sync_methods/{method}'
                this_controller[-1]['Synchronous methods'].append({f'{fix_name(method)}()':method_path})
        if os.path.isdir(f'./docs/{full_path}/listener_methods'):
            this_controller.append({'Listener methods':[]})
            for method in md_list(f'{full_path}/listener_methods'):
                method_path = f'{full_path}/listener_methods/{method}'
                this_controller[-1]['Listener methods'].append({f'{fix_name(method)}()':method_path})

    with open(OUTPUT_FILE, 'w') as file_w:
        yaml.dump(content, file_w, sort_keys=False)

with open(BASE_TREE_FILE) as file:
    content = yaml.load(file, Loader=yaml.FullLoader)
    # content['extra']['tree']['raya']['basic_controllers'] = {}
    basic_controllers = content['extra']['tree']['raya']['basic_controllers']

    for path in dir_list(BASIC_CONTROLLERS_PATH):
        full_path = f'{BASIC_CONTROLLERS_PATH}/{path}'
        controller = fix_name(path)
        basic_controllers[controller] = {}
        basic_controllers[controller][controller] = f'/{full_path}/'
        for page in md_list(full_path):
            basic_controllers[controller][fix_name(page)] = f'/{full_path}/{page[:-3]}/'
        if os.path.isdir(f'./docs/{full_path}/sync_methods'):
            basic_controllers[controller]['synchronous_methods'] = {}
            for method in md_list(f'{full_path}/sync_methods'):
                method_path = f'{full_path}/sync_methods/{method[:-3]}'
                basic_controllers[controller]['synchronous_methods'][fix_name(method)] = f'/{method_path}/'
        if os.path.isdir(f'./docs/{full_path}/listener_methods'):            
            basic_controllers[controller]['listener_methods'] = {}
            for method in md_list(f'{full_path}/listener_methods'):
                method_path = f'{full_path}/listener_methods/{method[:-3]}'
                basic_controllers[controller]['listener_methods'][fix_name(method)] = f'/{method_path}/'

    with open(OUTPUT_TREE_FILE, 'w') as file_w:
        yaml.dump(content, file_w, sort_keys=False)