# Raya Python Library Documentation

## Install python dependencies

``` bash
pip3 install mkdocs mkdocs-material mkdocs-macros-plugin
```

## To run locally

Run the followinf command in the repo root:

``` bash
python3 scripts/update_tree.py
```

## Start local server

``` bash
python3 -m mkdocs serve
```

Open the server in [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

## Before pushing

Run:

``` bash
sed -i 's/: \//: \/ur-docs\//g' data/doc_tree.yml
```