site_name: Unlimited Robotics
site_url: https://example.com/
theme:
  name: material
copyright: Copyright &copy; 2020 - 20XX Unlimited Robotics Ltd.
extra:
  social:
  - icon: fontawesome/brands/twitter
    link: https://twitter.com/UnlimitedRobots
  - icon: fontawesome/brands/linkedin
    link: https://www.linkedin.com/company/unlimited-robotics/
  - icon: fontawesome/brands/facebook
    link: https://www.facebook.com/unlimitedrobotic
plugins:
- search
- macros:
    include_yaml:
    - data/general_variables.yml
    - data/doc_tree.yml
    - data/messages.yml
    - data/exceptions.yml
markdown_extensions:
- pymdownx.highlight:
    use_pygments: true
- codehilite
- pymdownx.superfences
- toc:
    permalink: '#'
nav:
- Home: index.md
- Software Development Kit:
  - Software Development Kit: 1_sdk/1_sdk.md
  - Installation: 1_sdk/2_installation.md
  - Getting Started:
    - Command Line Interface: 1_sdk/getting_started/cli.md
    - VSCode Plugin: 1_sdk/getting_started/vscode.md
  - CLI - Reference: 1_sdk/4_cli_reference.md
- Raya - Python Library:
  - Raya - Python Library: 2_raya/1_raya.md
  - Raya Application Structure: 2_raya/2_python_app_struct.md
  - Hello World Application: 2_raya/3_hello_world_app.md
  - Controllers Schema: 2_raya/4_controllers_schema.md
  - Listeners Schema: 2_raya/5_listeners_schema.md
  - Basic Controllers:
    - Sensors controller:
      - Sensors controller: 2_raya/6_basic_controllers/1_sensors_controller/1_sensors_controller.md
      - List of sensors: 2_raya/6_basic_controllers/1_sensors_controller/2_list_of_sensors.md
      - Synchronous methods:
        - get_sensor_value(): 2_raya/6_basic_controllers/1_sensors_controller/sync_methods/1_get_sensor_value.md
        - get_all_sensors_values(): 2_raya/6_basic_controllers/1_sensors_controller/sync_methods/2_get_all_sensors_values.md
        - check_sensor_in_range(): 2_raya/6_basic_controllers/1_sensors_controller/sync_methods/3_check_sensor_in_range.md
        - check_color_sensor_in_range(): 2_raya/6_basic_controllers/1_sensors_controller/sync_methods/4_check_color_sensor_in_range.md
        - check_color_sensor_name(): 2_raya/6_basic_controllers/1_sensors_controller/sync_methods/5_check_color_sensor_name.md
      - Listener methods:
        - create_threshold_listener(): 2_raya/6_basic_controllers/1_sensors_controller/listener_methods/1_create_threshold_listener.md
        - create_boolean_listener(): 2_raya/6_basic_controllers/1_sensors_controller/listener_methods/2_create_boolean_listener.md
        - create_color_range_listener(): 2_raya/6_basic_controllers/1_sensors_controller/listener_methods/3_create_color_range_listener.md
        - create_color_name_listener(): 2_raya/6_basic_controllers/1_sensors_controller/listener_methods/4_create_color_name_listener.md
        - delete_listener(): 2_raya/6_basic_controllers/1_sensors_controller/listener_methods/5_delete_listener.md
    - Lidar controller:
      - Lidar controller: 2_raya/6_basic_controllers/2_lidar_controller/1_lidar_controller.md
      - Synchronous methods:
        - get_laser_info(): 2_raya/6_basic_controllers/2_lidar_controller/sync_methods/1_get_laser_info.md
        - get_raw_data(): 2_raya/6_basic_controllers/2_lidar_controller/sync_methods/2_get_raw_data.md
        - check_obstacle(): 2_raya/6_basic_controllers/2_lidar_controller/sync_methods/3_check_obstacle.md
      - Listener methods:
        - create_obstacle_listener(): 2_raya/6_basic_controllers/2_lidar_controller/listener_methods/1_create_obstacle_listener.md
        - delete_listener(): 2_raya/6_basic_controllers/2_lidar_controller/listener_methods/2_delete_listener.md
    - Cameras controller:
      - Cameras controller: 2_raya/6_basic_controllers/3_cameras_controller/1_cameras_controller.md
      - List of cameras: 2_raya/6_basic_controllers/3_cameras_controller/2_list_of_cameras.md
      - Synchronous methods:
        - get_available_cameras(): 2_raya/6_basic_controllers/3_cameras_controller/sync_methods/1_get_available_cameras.md
        - enable_camera(): 2_raya/6_basic_controllers/3_cameras_controller/sync_methods/2_enable_camera.md
        - disable_camera(): 2_raya/6_basic_controllers/3_cameras_controller/sync_methods/3_disable_camera.md
        - take_snapshot(): 2_raya/6_basic_controllers/3_cameras_controller/sync_methods/4_take_snapshot.md
      - Listener methods:
        - create_frame_listener(): 2_raya/6_basic_controllers/3_cameras_controller/listener_methods/1_create_frame_listener.md
        - delete_frame_listener(): 2_raya/6_basic_controllers/3_cameras_controller/listener_methods/2_delete_frame_listener.md
    - Motion controller:
      - Motion controller: 2_raya/6_basic_controllers/4_motion_controller/1_motion_controller.md
      - Synchronous methods:
        - set_velocity(): 2_raya/6_basic_controllers/4_motion_controller/sync_methods/1_set_velocity.md
        - set_linear_velocity(): 2_raya/6_basic_controllers/4_motion_controller/sync_methods/2_set_linear_velocity.md
        - set_rotational_velocity(): 2_raya/6_basic_controllers/4_motion_controller/sync_methods/3_set_rotational_velocity.md
        - get_command_status(): 2_raya/6_basic_controllers/4_motion_controller/sync_methods/4_get_command_status.md
        - stop_motion(): 2_raya/6_basic_controllers/4_motion_controller/sync_methods/5_stop_motion.md
      - Listener methods:
        - create_finished_command_listener(): 2_raya/6_basic_controllers/4_motion_controller/listener_methods/1_create_finished_command_listener.md
        - delete_listener(): 2_raya/6_basic_controllers/4_motion_controller/listener_methods/2_delete_listener.md
    - Arms controller:
      - Arms controller: 2_raya/6_basic_controllers/5_arms_controller/1_arms_controller.md
      - List of arms: 2_raya/6_basic_controllers/5_arms_controller/2_list_of_arms.md
      - Synchronous methods:
        - get_available_arms(): 2_raya/6_basic_controllers/5_arms_controller/sync_methods/1_get_available_arms.md
        - get_joints_list(): 2_raya/6_basic_controllers/5_arms_controller/sync_methods/2_get_joints_list.md
        - run_trajectory(): 2_raya/6_basic_controllers/5_arms_controller/sync_methods/3_run_trajectory.md
        - get_trajectory_status(): 2_raya/6_basic_controllers/5_arms_controller/sync_methods/4_get_trajectory_status.md
      - Listener methods:
        - create_finished_trajectory_listener(): 2_raya/6_basic_controllers/5_arms_controller/listener_methods/1_create_finished_trajectory_listener.md
        - delete_listener(): 2_raya/6_basic_controllers/5_arms_controller/listener_methods/2_delete_listener.md
  - Skills Controllers: []
  - Extras:
    - Raya Exceptions: 2_raya/8_extras/raya_exceptions.md
    - List of Predefined Colors: 2_raya/8_extras/list_of_colors.md
